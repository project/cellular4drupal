/**
 * @file
 * Configure grunt concat.
 *
 * Requires grunt-contrib-concat + .lib/language
 * https://github.com/ablank/grunt-contrib-concat
 */

module.exports = {
  theme_info: {
    src: [
      'src/info/info.txt',
      'src/info/opts.txt'
    ],
    dest: 'cellular.info'
  },
  templatephp: {
    options: {
      banner: '/**\n\n\
    * @file\n\
    * Subcellular Theme for Drupal 7.\n\
    *\n\
    * @author Adam Blankenship\n\
    * \n\
    * @see http://live-cellular.gotpantheon.com\n\
    * @see https://github.com/ablank/cellular4drupal\n\
    */\n\n',
      language: {
        type: 'php'
      }
    },
    src: [
      // 'src/preprocess/*.inc',
      'src/preprocess/_init.inc',
      'src/preprocess/alter.inc',
      'src/preprocess/alter_css.inc',
      'src/preprocess/alter_js.inc',
      'src/preprocess/preprocess_html.inc',
      'src/preprocess/preprocess.inc'
    ],
    dest: 'template.php'
  }
};
